# Mirros::Widget::Iframe
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'mirros-widget-embed_iframe'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install mirros-widget-embed_iframe
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
