# frozen_string_literal: true

module Mirros
  module Widget
    module EmbedIframe
      class Engine < ::Rails::Engine
        isolate_namespace Mirros::Widget::EmbedIframe
        config.generators.api_only = true
      end
    end
  end
end
