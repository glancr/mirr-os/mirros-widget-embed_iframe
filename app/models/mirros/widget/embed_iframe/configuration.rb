# frozen_string_literal: true
module Mirros
  module Widget
    module EmbedIframe
      class Configuration < WidgetInstanceConfiguration

        class EmbeddableUrlValidator < ActiveModel::EachValidator
          def validate_each(record, attribute, value)
            uri = URI.parse(value)
            if uri.is_a?(URI::HTTP) && uri.host.present?
              headers = HTTParty.head(uri).headers
              record.errors.add "configuration/#{attribute}", I18n.t('embed_iframe.no_embedding_allowed_message', url: value) if headers['x-frame-options'].present?
            else
              record.errors.add "configuration/#{attribute}", I18n.t("The given URL #{value} is not an HTTP URL or does not contain a host name.")
            end

          rescue URI::InvalidURIError => error
            record.errors.add "configuration/#{attribute}", I18n.t('embed_iframe.input_url_is_not_valid_message', url: value, message: error.message)
          end
        end

        attribute :url, :string, default: ''
        validates :url, embeddable_url: true, allow_blank: true
      end
    end
  end
end
